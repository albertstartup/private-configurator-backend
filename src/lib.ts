import _ from 'underscore'
import Shopify, { IMetafield, IPrivateShopifyConfig, IPublicShopifyConfig } from 'shopify-api-node'
import { Express, Request, Response, NextFunction } from 'express'
import boom from 'boom'
import * as R from 'ramda'
import mongodb from 'mongodb'

export type StoreMetafields = IMetafield[]

export type OptionValueName = string
export type OptionName = string

type InvalidatedOptionValueName = string

export type Invalidates = {
  [invalidatedOptionName: string]: InvalidatedOptionValueName[]
}

export type OptionValues = {
  [optionValueName: string]: {
    id: OptionValueName,
    invalidates: Invalidates
  }
}

export type GlobalOptions = {
  [optionName: string]: OptionValues
}

export type ProductOptions = {
  [optionName: string]: OptionValueName[]
}

export const getGlobalOptionsMetafield = (metafields: IMetafield[]): IMetafield => {
  return _.findWhere(metafields, { key: 'globalOptions' });
}

export const fetchShopMetafields = async (shopify: Shopify) => {
  try {
    const shopMetafields = await shopify.metafield.list({
      metafield: { owner_resource: 'shop' }
    })
    return shopMetafields
  } catch (error) {
    throw error
  }
}

export const getProductDescriptionMetafield = (metafields: IMetafield[]): IMetafield => {
  return _.findWhere(metafields, { key: 'productDescription' });
}

export const getDefaultStateMetafield = (metafields: IMetafield[]): IMetafield => {
  return _.findWhere(metafields, { key: 'configuratorDefaultState' });
};

export const extractProductOptions = (metafields: IMetafield[]): ProductOptions => {
  return extractJSONStringMetafieldValue(metafields, 'productOptions')
}

export const extractGlobalOptions = (storeMetafields: StoreMetafields): GlobalOptions => {
  return extractJSONStringMetafieldValue(storeMetafields, 'globalOptions')
}

export const extractJSONStringMetafieldValue = (metafields: IMetafield[], metafieldKey: string): Partial<{}> => {
  const metafield = R.find((metafield) => {
    return metafield.key === metafieldKey
  }, metafields)
  if (typeof metafield.value !== 'string') { throw Error('metafield value is not a string.') }
  return JSON.parse(metafield.value)
}

export const fetchProductMetafields = async (shopify: Shopify, productId: string): Promise<IMetafield[]> => {
  try {
    const metafields = await shopify.metafield.list({
      metafield: { owner_resource: 'product', owner_id: productId }
    })

    return metafields
  } catch (error) {
    throw error
  }    
}

export const fetchProductOptions = async (shopify: Shopify, productId: string): Promise<ProductOptions> => {
  const product_metafields = await fetchProductMetafields(shopify, productId)
  return extractProductOptions(product_metafields)
}

export type JSON_STRING = string

// Todo: use typescript
export enum MetafieldOwnerResource {
  PRODUCT = 'product',
  SHOP = 'shop'
}

export enum MetafieldValueType {
  JSON_STRING = 'json_string',
  STRING = 'string'
}

export const createMetafield = async (shopify: any,
  key: string, value: {}, value_type: MetafieldValueType,
  owner_resource: MetafieldOwnerResource, ownerId?: string, namespace="configuratorApp"): Promise<IMetafield> => {

  type CreateMetafieldParamsType = {
    key: string,
    value: {},
    owner_resource: string,
    ownerId?: string,
    namespace: string
  }

  const createMetafieldParams: CreateMetafieldParamsType = {
    key: key,
    value: value,
    owner_resource: owner_resource,
    namespace: namespace
  }
  if (owner_resource === MetafieldOwnerResource.PRODUCT && ownerId) {
    createMetafieldParams.ownerId = ownerId
  }

  try {
    const createdMetafield = await shopify.metafield.create(createMetafieldParams)
    return createdMetafield
  } catch (error) {
    throw error
  }
}

export const setProductCategoriesMetafield = async (shopify: Shopify, productCategories: string[]) => {
  const metafieldValue = JSON.stringify(productCategories)
  try {
    const createdMetafield = await shopify.metafield.create({
      key: 'productCategories',
      value: metafieldValue,
      value_type: 'json_string',
      namespace: 'inpr',
      owner_resource: 'shop'
    })
    return createMetafield
  } catch (error) {
    throw error
  }
}

export const fetchGlobalOptions = async (shopify: Shopify): Promise<GlobalOptions> => {
  try {
    const shopMetafields = await fetchShopMetafields(shopify)

    const globalOptions = extractGlobalOptions(shopMetafields)

    return globalOptions
  } catch (error) {
    throw error
  }
}

export const asyncMiddleware = (fn: (req: Request, res: Response, next: NextFunction) => Promise<any>) => (req: Request, res: Response, next: NextFunction) => {
  Promise.resolve(fn(req, res, next)).catch((err) => {
    if (!err.isBoom) {
      return next(boom.badImplementation(err));
    }
    next(err);
  });
};

export const getMongoDatabase = async (mongo_url: string, ): Promise<mongodb.Db>  => { 
  console.log(mongo_url)
                     
  const client = await mongodb.MongoClient.connect(mongo_url)

  const db = client.db()

  return db
}

export const errorHandler = (error: boom, req: Request, res: Response, next: NextFunction) => {
  if (error.isServer) {
    console.error('error :', error);
  }

  return res.status(error.output.statusCode).send(error.output.payload)
}