import { Express, Request, Response, NextFunction } from 'express'
import Shopify, { IProduct } from 'shopify-api-node'
import boom from 'boom'
import { asyncMiddleware, getMongoDatabase } from './lib'
import { mongo_url } from './mongoSetup';

module.exports = function (app: Express, shopify: Shopify) {

    app.get('/api/products', asyncMiddleware(async (req: Request, res: Response, next: NextFunction) => {
        try {
            const products: IProduct[] = await shopify.product.list()
            res.send(products)
        } catch (error) {
            throw boom.badRequest(error)
        }
    }))

    app.post('/api/createProduct', asyncMiddleware(async (req: Request, res: Response, next: NextFunction) => {
        const { name, productTypeId, printTypeId, productCategoryId } = req.body

        try {
            const db = await getMongoDatabase(mongo_url)
            const response = await db.collection('products').insertOne({ name, productTypeId, printTypeId, productCategoryId })
            if (response.result.ok === 1) {
                res.send(response.ops[0])
            } else {
                throw boom.badRequest()
            }
        } catch (error) {
            throw error
        }
    }))
    
    app.get('/api/product/:productId', asyncMiddleware(async (req: Request, res: Response, next: NextFunction) => {
        const productId = req.params['productId'];

        try {
            const product = await shopify.product.get(productId);
            res.send(product);
        } catch (err) {
            //res.send({ error: err });

        }
    }))

    // app.post('/api/createProduct', (req: Request, res: Response, next: NextFunction) => {
    //     if (req.body && req.body.productName &&
    //         req.body.configuratorDefaultState && req.body.productDescription) {
    //         //debugger;
    //         shopify.product.create({
    //             "title": req.body.productName,
    //             "metafields": [
    //                 {
    //                     key: 'configuratorDefaultState',
    //                     value: req.body.configuratorDefaultState,
    //                     value_type: 'json_string',
    //                     namespace: 'inpr'
    //                 },
    //                 {
    //                     key: 'productDescription',
    //                     value: req.body.productDescription,
    //                     value_type: 'json_string',
    //                     namespace: 'inpr'
    //                 }
    //             ]
    //         }).then((response) => {
    //             res.send(response)
    //         }).catch((err) => {
    //             res.send({ error: err })
    //         });
    //     } else {
    //         res.send({ error: 'Missing json items' });
    //     }
    // })

    app.post('/api/updateProductName/:productId/:newName', async (req: Request, res: Response, next: NextFunction) => {
        const productId = req.params['productId'];
        const newName = req.params['newName']

        try {
            const response = await shopify.product
                .update(productId, {
                    title: newName
                })

            res.send(response)
        } catch (error) {
            next(error)
        }
    })

}