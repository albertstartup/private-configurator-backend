import { Express, Request, Response, NextFunction } from 'express'
import Shopify, { IProduct } from 'shopify-api-node'
import { getGlobalOptionsMetafield, asyncMiddleware, fetchGlobalOptions, getMongoDatabase } from './lib';
import boom from 'boom';
import { mongo_url } from './mongoSetup';

module.exports = function (app: Express, shopify: Shopify) {

    app.post('/api/createProductOption/:optionName', asyncMiddleware(async (req: Request, res: Response, next: NextFunction) => {
        try {
            const optionName: string = req.params.optionName

            if (!optionName) { throw boom.badRequest() }            

            const db = await getMongoDatabase(mongo_url)

            const response = await db.collection('productOptions').insert({ name: optionName })

            if (response.result.ok === 1) {
                res.send({ createdId: response.ops[0]._id })
            } else {
                throw boom.badRequest()
            }
        } catch (error) {
            throw error
        }
    }))

    app.post('/api/createOption/:optionName', asyncMiddleware(async (req: Request, res: Response, next: NextFunction) => {
        try {
            const optionName: string = req.params.optionName

            if (!optionName) { throw boom.badRequest() }



        } catch (error) {
            throw error
        }
    }))

    app.post('/updateGlobalOptions', async (req: Request, res: Response, next: NextFunction) => {
        const newGlobalOptions = JSON.stringify(req.body);

        try {
            const metafields = await shopify.metafield.list({
                metafield: { owner_resource: 'shop' }
            });

            const globalOptionsMf = getGlobalOptionsMetafield(metafields);

            if (globalOptionsMf) {
                shopify.metafield.update(globalOptionsMf.id, {
                    id: globalOptionsMf.id,
                    value: newGlobalOptions,
                    value_type: 'json_string'
                }).then((mf) => {
                    //debugger;
                    console.log('updated');
                    res.send(mf);
                }).catch((err) => {
                    res.send(err);
                });
            }
        } catch (err) {
            res.send({ error: err })
        }
    })

    app.get('/getGlobalOptions', async (req: Request, res: Response, next: NextFunction) => {

    });

    app.get('/getProductCategories', asyncMiddleware(async () => {
        const shopMetafields = await fetchGlobalOptions(shopify)        
    }))    

    // const fetchOptions = async () => {
    //     try {
    //         const shopMetafields = await shopify.metafield.list({
    //             metafield: { owner_resource: 'shop' }
    //         });

    //        const globalOptionsMf = getGlobalOptionsMetafield(metafields);

    //         if (globalOptionsMf) {
    //             res.send(globalOptionsMf.value)
    //         }
    //     } catch (err) {
    //         res.send({ error: err })
    //     }
    // }
}