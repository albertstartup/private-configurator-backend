const environment = process.env.NODE_ENV || 'dev'

export let mongo_url: string

if (environment === 'production') {
    mongo_url = process.env.MONGODB_URI
} else if (environment === 'dev') {
    mongo_url = 'mongodb://127.0.0.1:27017/configurator1'
}

export default {}