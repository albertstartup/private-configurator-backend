import { Express, Request, Response, NextFunction } from 'express'
import Shopify, { IProduct } from 'shopify-api-node'
import { getGlobalOptionsMetafield, asyncMiddleware, fetchGlobalOptions, getMongoDatabase} from './lib';
import mongodb from 'mongodb'
import { mongo_url } from './mongoSetup'
import boom from 'boom'

module.exports = function (app: Express, shopify: Shopify) {

    app.post('/api/createProductCategory/:categoryName', asyncMiddleware(async (req: Request, res: Response, next: NextFunction) => {
        try {
            const categoryName = req.params['categoryName']

            const db = await getMongoDatabase(mongo_url)

            const response = await db.collection('productCategories').insertOne({ name: categoryName })

            if (response.result.ok === 1) {
                res.send(response.ops[0])
            } else {
                throw boom.badRequest()
            }
        } catch (error) {
            throw error
        }
    }))

    app.get('/api/getProductCategories', asyncMiddleware(async (req: Request, res: Response, next: NextFunction) => {
        try {
            const db = await getMongoDatabase(mongo_url)

            const cursor = await db.collection('productCategories').find()

            const results: any[] = await cursor.map((doc) => {
                return doc
            }).toArray()
                        
            res.send(results)
        } catch (error) {
            throw error
        }
    }))

    app.get('/')
}