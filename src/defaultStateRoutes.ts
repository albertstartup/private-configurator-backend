import {Express, Request, Response, NextFunction} from 'express'
import Shopify, { IProduct } from 'shopify-api-node'
import boom from 'boom'
import { getDefaultStateMetafield } from './lib';

module.exports = function(app: Express, shopify: Shopify) {
    app.get('/getDefaultState/:productId', (req: Request, res: Response, next: NextFunction) => {
        const productId = req.params['productId'];
    
        shopify.metafield.list({
            metafield: { owner_resource: 'product', owner_id: productId }
        }).then((mfs) => {
            const defaultStateMetafield = getDefaultStateMetafield(mfs);
            if (defaultStateMetafield) {
                console.log('defaultState: ', defaultStateMetafield);
                res.send(defaultStateMetafield);
            }
        }).catch(error => {
            //res.send(err);
            //console.log(err);
            next(boom.badRequest('Missing or incorrect parameter.'))
        });
    });
    
    app.post('/setDefaultState/:productId', (req: Request, res: Response, next: NextFunction) => {
        const productId = req.params['productId'];
        const configuratorDefaultState = JSON.stringify(req.body);
    
        if (req.body && req.body.selections && req.body.options) {
            shopify.metafield.create({
                key: 'configuratorDefaultState',
                value: configuratorDefaultState,
                value_type: 'json_string',
                namespace: 'inpr',
                owner_resource: 'product',
                owner_id: productId
            }).then(mf => {
                res.send(mf);
                console.log('mf: ', mf);
            }).catch(err => {
                res.send(err);
                console.log(err)
            });
        } else {
            res.send({ error: 'Incorrect req' })
        }
    });
    
    app.post('/updateDefaultState/:productId', async (req: Request, res: Response, next: NextFunction) => {
        const productId = req.params['productId'];
        const configuratorDefaultState = JSON.stringify(req.body);
    
        try {
            const results = await shopify.metafield.list({
                metafield: { owner_resource: 'product', owner_id: productId }
            });
    
            const defaultStateMetafield = results && getDefaultStateMetafield(results);
    
            if (defaultStateMetafield.id) {
                shopify.metafield.update(defaultStateMetafield.id, {
                    // TODO should id go here like globalOptions
                    value: configuratorDefaultState,
                    value_type: 'json_string'
                }).then((mf) => {
                    console.log('updated');
                    res.send(mf);
                }).catch((err) => {
                    res.send(err);
                });
            }
    
        } catch (err) {
            console.log(err);
            res.send(err);
        }
    });
}